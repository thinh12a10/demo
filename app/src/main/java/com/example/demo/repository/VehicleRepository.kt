package com.example.demo.repository

import com.example.demo.model.Vehicle
import com.example.demo.model.VehicleFactory

class VehicleRepository {

    fun getVehicles(): MutableList<Vehicle> {
        val result = mutableListOf<Vehicle>()
        for (i in 1..20) {
            val type = (1..3).random()
            result.add(VehicleFactory.makeVehicle(type))
        }
        return result
    }
}