package com.example.demo.data.enum

enum class VehicleType(val id: Int) {
    CAR(1),
    MOTOR_CYCLE(2),
    BIKE(3)
}