package com.example.demo.ui.top

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.demo.R
import com.example.demo.databinding.FragmentTopBinding
import com.example.demo.widgets.GridSpacingItemDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel

class TopFragment : Fragment() {

    private val viewModel: TopViewModel by viewModel()
    private var _binding: FragmentTopBinding? = null
    private val binding get() = _binding!!
    private val adapter = VehicleAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentTopBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        observerData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initAdapter() {
        binding.rcvVehicles.adapter = adapter
        val spacing = resources.getDimensionPixelSize(R.dimen.dp_10)
        binding.rcvVehicles.addItemDecoration(GridSpacingItemDecoration(spacing, 2))
    }

    private fun observerData() {
        viewModel.vehicles.observe(viewLifecycleOwner) {
            adapter.replaceData(it)
        }
    }
}