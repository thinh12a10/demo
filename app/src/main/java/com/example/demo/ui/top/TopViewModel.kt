package com.example.demo.ui.top

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.demo.model.Vehicle
import com.example.demo.repository.VehicleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TopViewModel(
    private val vehicleRepository: VehicleRepository
) : ViewModel() {
    val vehicles = MutableLiveData<MutableList<Vehicle>>()

    init {
        getVehicles()
    }

    fun getVehicles() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = vehicleRepository.getVehicles()
            vehicles.postValue(response)
        }
    }
}