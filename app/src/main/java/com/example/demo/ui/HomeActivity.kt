package com.example.demo.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.R
import com.example.demo.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    lateinit var viewPagerAdapter: HomeViewPagerAdapter
    lateinit var viewBinding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        initialize()
    }

    private fun initialize() {
        viewPagerAdapter = HomeViewPagerAdapter(supportFragmentManager, lifecycle)
        viewBinding.viewPagerContainer.adapter = viewPagerAdapter
        viewBinding.viewPagerContainer.isUserInputEnabled = false
        viewBinding.bottomBar.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.itemHome -> {
                    viewBinding.viewPagerContainer.setCurrentItem(0, true)
                }
                R.id.itemProfile -> {
                    viewBinding.viewPagerContainer.setCurrentItem(1, true)
                }
            }
            true
        }
    }
}