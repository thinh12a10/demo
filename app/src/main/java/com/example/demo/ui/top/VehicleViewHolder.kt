package com.example.demo.ui.top

import androidx.recyclerview.widget.RecyclerView
import com.example.demo.R
import com.example.demo.databinding.ItemVehicleBinding
import com.example.demo.model.Vehicle

class VehicleViewHolder(
    private val viewBinding: ItemVehicleBinding
) : RecyclerView.ViewHolder(viewBinding.root) {

    fun bindView(data: Vehicle) {
        with(viewBinding) {
            imgVehicle.setImageResource(data.getVehicleImage())
            tvName.text = data.getVehicleName()
            tvPrice.text = viewBinding.root.resources.getString(R.string.price_format, data.getVehiclePrice())
        }
    }
}