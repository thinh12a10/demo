package com.example.demo.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.demo.ui.profile.ProfileFragment
import com.example.demo.ui.top.TopFragment

class HomeViewPagerAdapter : FragmentStateAdapter {

    constructor(fragmentActivity: FragmentActivity) : super(fragmentActivity) {
        initData()
    }

    constructor(fragment: Fragment) : super(fragment) {
        initData()
    }

    constructor(fragmentManager: FragmentManager, lifecycle: Lifecycle) : super(fragmentManager, lifecycle) {
        initData()
    }

    private val fragments = ArrayList<Fragment>()

    private fun initData() {
        fragments.add(TopFragment())
        fragments.add(ProfileFragment())
    }

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}