package com.example.demo.ui.top

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.demo.databinding.ItemVehicleBinding
import com.example.demo.model.Vehicle

class VehicleAdapter : RecyclerView.Adapter<VehicleViewHolder>() {

    private val vehicles = mutableListOf<Vehicle>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val binding = ItemVehicleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VehicleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        vehicles.getOrNull(position)?.let {
            holder.bindView(it)
        }
    }

    override fun getItemCount(): Int {
        return vehicles.size
    }

    fun replaceData(data: MutableList<Vehicle>) {
        vehicles.clear()
        vehicles.addAll(data)
        notifyDataSetChanged()
    }
}