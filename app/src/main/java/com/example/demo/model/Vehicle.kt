package com.example.demo.model

interface Vehicle {
    fun getVehicleName(): String
    fun getVehiclePrice(): Long
    fun getVehicleImage(): Int
}