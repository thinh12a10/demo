package com.example.demo.model

import com.example.demo.R

class Bike : Vehicle {
    override fun getVehicleName(): String {
        return "Bike"
    }

    override fun getVehiclePrice(): Long {
        return 100
    }

    override fun getVehicleImage(): Int {
        return R.drawable.bike
    }
}