package com.example.demo.model

import com.example.demo.R

class MotorCycle : Vehicle {
    override fun getVehicleName(): String {
        return "Motor cycle"
    }

    override fun getVehiclePrice(): Long {
        return 1000
    }

    override fun getVehicleImage(): Int {
        return R.drawable.motor_cycle
    }
}