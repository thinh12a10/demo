package com.example.demo.model

class Bill(
    private var nameVehicle: String? = null,
    private var price: Int = 0,
    private var numberVehicles: Int = 0,
    private var discount: Float = 0.0f
) {
    private val totalPrice get() = price * numberVehicles * (1 - discount)

    class Builder {
        private var nameVehicle: String? = null
        private var price: Int = 0
        private var numberVehicles: Int = 0
        private var discount: Float = 0.0f

        fun setNameVehicle(name: String) = apply {
            nameVehicle = name
        }

        fun setPrice(price: Int) = apply {
            this.price = price
        }

        fun setNumberVehicles(numberVehicles: Int) = apply {
            this.numberVehicles = numberVehicles
        }

        fun setDiscount(discount: Float) = apply {
            this.discount = discount
        }

        fun build() = Bill(nameVehicle, price, numberVehicles, discount)
    }
}