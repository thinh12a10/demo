package com.example.demo.model

import com.example.demo.R

class Car : Vehicle {
    override fun getVehicleName(): String {
        return "Car"
    }

    override fun getVehiclePrice(): Long {
        return 10000
    }

    override fun getVehicleImage(): Int {
        return R.drawable.car
    }
}