package com.example.demo.model

import com.example.demo.data.enum.VehicleType

object VehicleFactory {
    fun makeVehicle(typeId: Int): Vehicle {
        return when (typeId) {
            VehicleType.CAR.id -> Car()
            VehicleType.MOTOR_CYCLE.id -> MotorCycle()
            else -> Bike()
        }
    }
}