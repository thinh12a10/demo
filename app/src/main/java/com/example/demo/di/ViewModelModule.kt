package com.example.basickotlin.di

import com.example.demo.ui.top.TopViewModel
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel<TopViewModel>()
}
