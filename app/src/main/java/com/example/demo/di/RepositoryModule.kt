package com.example.basickotlin.di

import com.example.demo.repository.VehicleRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { VehicleRepository() }
}
