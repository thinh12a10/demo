package com.example.basickotlin.app

import android.app.Application
import com.example.basickotlin.di.repositoryModule
import com.example.basickotlin.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(listOf(viewModelModule, repositoryModule))
        }
    }
}